import React, { Component } from 'react';



class ListItem extends Component {
  constructor(props) {
      super(props)
      this.removeItem = this.removeItem.bind(this);
  }

  removeItem() {
      this.props.remove(this.props.itemName);
  }

  render() {
    return (
      <li style={{textAlign : "left"}} >
        <input name={this.props.itemName} type="checkbox" value={this.props.itemValue} onClick={this.removeItem}/>{this.props.itemValue}
      </li>
    );
  }
}

export default ListItem;