import React, { Component } from 'react';



class NewItem extends Component {
  constructor(props) {
    super(props)
    this.addItem = this.addItem.bind(this);
    this.state = {
      itemValue: "",
      itemName: "",
    }
    this.addItem = this.addItem.bind(this);
    this.handleItemUpdate = this.handleItemUpdate.bind(this);
  }

  addItem() {
    if (this.state.itemValue !== "") {
      this.props.add(this.state);
    }
    this.setState({itemValue: "", itemName: ""});
  }

  handleItemUpdate(event) {
    this.setState({
      itemValue: event.target.value,
      itemName: event.target.value
    });
  }

  render() {
    return (
      <div algin="left">
        <input  name="newItem" type="text"  value={this.state.itemValue} onChange={this.handleItemUpdate}/>
        <button  onClick={this.addItem}>Add</button>
      </div>
    );
  }
}

export default NewItem;