import React, { Component } from 'react';
import ListItem from './ListItem';
import NewItem from './NewItem';


class ToDoList extends Component {
  constructor(prop) {
    super(prop)
    this.state = {
      itemNumber : 0,
      mylist : []
    }
    this.remove=this.remove.bind(this);
    this.add=this.add.bind(this);
  }

  remove(itemName) {
    this.setState({mylist: this.state.mylist.filter((listitem)=> (listitem.itemName !== itemName) )});
  }

  add(item) {
    item.itemName = item.itemName + this.state.itemNumber;
    this.setState({mylist: this.state.mylist.concat(item),
                   itemNumber: this.state.itemNumber +1});
  }

  render() {
    return (
      <div className="todo">
        <table align="center">
          <tbody>
            <tr>
              <td>
                <NewItem
                  add={this.add}
                />
                <ul style={{ listStyle: "none" }}>
                  {
                    this.state.mylist.map((item) => {
                      return ((
                        <ListItem
                          itemName={item.itemName}
                          itemValue={item.itemValue}
                          remove={this.remove}
                          key={item.itemName}
                        />
                      ))
                    })
                  }
                </ul>

              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default ToDoList;